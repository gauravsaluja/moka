package com.gauravsaluja.domain.interactors

import com.gauravsaluja.domain.executor.Executor
import com.gauravsaluja.domain.executor.PostExecutionThread
import com.gauravsaluja.domain.model.ItemData
import com.gauravsaluja.domain.repository.ItemRepository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Use case for getting item list
 */

class GetItemListUseCase @Inject
constructor(
    threadExecutor: Executor, postExecutionThread: PostExecutionThread,
    private val itemRepository: ItemRepository
) : UseCase<List<ItemData>, Map<String, Any>>(threadExecutor, postExecutionThread) {

    // build observable for getting item data response
    override fun buildUseCaseObservable(): Observable<List<ItemData>> {
        return itemRepository.getItemsList()
    }
}