package com.gauravsaluja.domain.interactors

import com.gauravsaluja.domain.executor.PostExecutionThread
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

import java.util.concurrent.Executor

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Base class for use case
 */

abstract class UseCase<T, Params>(
    private var mThreadExecutor: Executor,
    private var mPostExecutionThread: PostExecutionThread
) {
    private var disposables: CompositeDisposable? = null
    private var isDisposed = true

    init {
        this.disposables = CompositeDisposable()
    }

    abstract fun buildUseCaseObservable(): Observable<T>

    // main executor function to take the observable and request parameters
    // background threads are taken from mThreadExecutor
    // main thread is taken from mPostExecutionThread
    fun execute(disposableObserver: DisposableObserver<T>) {
        this.buildUseCaseObservable()
            .subscribeOn(Schedulers.from(mThreadExecutor))
            .observeOn(mPostExecutionThread.scheduler)
            .subscribe(disposableObserver)

        addDisposable(disposableObserver)
    }

    // dispose function to be called on onDestroy
    fun dispose() {
        isDisposed = true
        if (!disposables!!.isDisposed) {
            disposables!!.dispose()
            disposables = CompositeDisposable()
        }
    }

    private fun addDisposable(disposable: Disposable) {
        isDisposed = false
        disposables!!.add(disposable)
    }
}