package com.gauravsaluja.domain.utils

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Constants file for domain module
 */

class Constants {

    companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    }
}