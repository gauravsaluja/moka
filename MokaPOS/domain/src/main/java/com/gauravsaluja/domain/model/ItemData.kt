package com.gauravsaluja.domain.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

class ItemData : Serializable {

    @SerializedName("albumId")
    @Expose
    var albumId: Int? = null
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("url")
    @Expose
    var url: String? = null
    @SerializedName("thumbnailUrl")
    @Expose
    var thumbnailUrl: String? = null

    companion object {
        private const val serialVersionUID = 5651489252313646559L
    }
}