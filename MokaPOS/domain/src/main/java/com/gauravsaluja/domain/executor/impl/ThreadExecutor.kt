package com.gauravsaluja.domain.executor.impl

import com.gauravsaluja.domain.executor.Executor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Extension of Executor class to define pool properties and configuration
 */

class ThreadExecutor @Inject
constructor() : Executor {

    private val mThreadPoolExecutor: ThreadPoolExecutor

    init {
        mThreadPoolExecutor = ThreadPoolExecutor(
            CORE_POOL_SIZE,
            MAX_POOL_SIZE,
            KEEP_ALIVE_TIME.toLong(),
            TIME_UNIT,
            WORK_QUEUE
        )
    }

    override fun execute(runnable: Runnable) {
        this.mThreadPoolExecutor.execute(runnable)
    }

    companion object {

        private const val CORE_POOL_SIZE = 3
        private const val MAX_POOL_SIZE = 5
        private const val KEEP_ALIVE_TIME = 120
        private val TIME_UNIT = TimeUnit.SECONDS
        private val WORK_QUEUE = LinkedBlockingQueue<Runnable>()
    }
}