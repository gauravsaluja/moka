package com.gauravsaluja.domain.interactors

import io.reactivex.observers.DisposableObserver

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Base class for use case observer
 */

open class UseCaseObserver<T> : DisposableObserver<T>() {

    override fun onNext(t: T) {

    }

    override fun onError(e: Throwable) {

    }

    override fun onComplete() {

    }
}