package com.gauravsaluja.domain.executor

import io.reactivex.Scheduler

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * PostExecutionThread interface, to be used for main / UI threads
 */

interface PostExecutionThread {

    val scheduler: Scheduler
}