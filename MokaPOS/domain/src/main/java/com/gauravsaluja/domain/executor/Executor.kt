package com.gauravsaluja.domain.executor

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Executor interface, to be used for background threads
 */

interface Executor : java.util.concurrent.Executor