package com.gauravsaluja.domain.repository

import com.gauravsaluja.domain.model.ItemData
import io.reactivex.Observable

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Repository interface
 */

interface ItemRepository {

    // get items list
    fun getItemsList(): Observable<List<ItemData>>
}