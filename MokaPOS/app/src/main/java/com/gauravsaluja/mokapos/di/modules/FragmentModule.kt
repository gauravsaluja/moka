package com.gauravsaluja.mokapos.di.modules

import android.app.Activity
import android.support.v4.app.Fragment
import com.gauravsaluja.mokapos.di.scope.PerFragment
import dagger.Module
import dagger.Provides

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Fragment module
 */

@Module
class FragmentModule(private val mFragment: Fragment) {

    @Provides
    @PerFragment
    internal fun providesFragment(): Fragment {
        return mFragment
    }

    @Provides
    @PerFragment
    internal fun provideActivity(): Activity? {
        return mFragment.activity
    }
}