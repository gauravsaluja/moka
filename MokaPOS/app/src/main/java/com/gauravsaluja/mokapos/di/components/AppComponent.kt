package com.gauravsaluja.mokapos.di.components

import android.app.Application
import com.gauravsaluja.data.api.ItemsApi
import com.gauravsaluja.domain.executor.Executor
import com.gauravsaluja.domain.executor.PostExecutionThread
import com.gauravsaluja.domain.repository.ItemRepository
import com.gauravsaluja.mokapos.di.modules.AppModule
import com.gauravsaluja.mokapos.di.modules.NetworkModule
import com.google.gson.Gson
import dagger.Component
import okhttp3.Cache
import okhttp3.OkHttpClient
import javax.inject.Singleton

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Application component
 */

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {

    fun providesApplication(): Application

    fun provideThreadExecutor(): Executor

    fun providePostExecutionThread(): PostExecutionThread

    fun provideItemRepository(): ItemRepository

    fun provideOkHttpCache(): Cache

    fun provideGson(): Gson

    fun provideOkHttpClient(): OkHttpClient

    fun provideItemsApi(): ItemsApi
}