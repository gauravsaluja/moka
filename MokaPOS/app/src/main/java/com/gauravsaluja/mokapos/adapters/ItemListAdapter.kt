package com.gauravsaluja.mokapos.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gauravsaluja.data.entity.Item
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.viewholder.ItemViewHolder
import kotlinx.android.synthetic.main.all_item_detail.view.*
import java.text.DecimalFormat
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

class ItemListAdapter @Inject
constructor() :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val itemDataList: MutableList<Item> = ArrayList()
    private lateinit var onClickItem: OnClickItem

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val viewItem = inflater.inflate(R.layout.all_item_detail, parent, false)
        return ItemViewHolder(viewItem)
    }

    fun setOnClickListener(context: Context) {
        onClickItem = context as OnClickItem
    }

    override fun getItemCount(): Int {
        return itemDataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemLayout = holder.itemView.item_layout
        val itemName = holder.itemView.item_name
        val itemPrice = holder.itemView.item_price

        val item = getItem(position)

        itemName.text = itemName.context.getString(R.string.item_name, item.id.toString())

        val price = item.price!!
        if (Math.ceil(price) == Math.floor(price) && price < Integer.MAX_VALUE) {
            val decimalFormat = DecimalFormat("0.#")
            itemPrice.text = itemPrice.context.getString(R.string.price, decimalFormat.format(price))
        } else {
            itemPrice.text = itemPrice.context.getString(R.string.price, String.format("%.2f", price))
        }

        itemLayout.setOnClickListener {
            onClickItem.clickItem(position, item)
        }
    }

    interface OnClickItem {
        fun clickItem(position: Int, item: Item)
    }

    private fun getItem(position: Int): Item {
        return itemDataList[position]
    }

    fun add(itemData: Item) {
        itemDataList.add(itemData)
        notifyItemInserted(itemDataList.size - 1)
    }

    fun addAll(itemDataList: List<Item>) {
        for (product in itemDataList) {
            add(product)
        }
    }
}