package com.gauravsaluja.mokapos.presenters

import com.gauravsaluja.domain.model.ItemData
import com.gauravsaluja.mokapos.base.BasePresenter

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

interface ItemsPresenter : BasePresenter {

    interface View {
        fun onItemsLoading()

        fun onItemsLoaded(itemDataResponse: List<ItemData>)

        fun onItemsFailed()
    }

    // function to bind view and contract
    fun setView(view: View)

    // load function for getting item list
    @Throws(Exception::class)
    fun load()
}