package com.gauravsaluja.mokapos.di.scope

import javax.inject.Scope

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Activity scope
 */

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity