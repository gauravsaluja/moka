package com.gauravsaluja.mokapos.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.adapters.LibraryAdapter
import com.gauravsaluja.mokapos.base.BaseFragment
import com.gauravsaluja.mokapos.dummy.LibraryContent
import kotlinx.android.synthetic.main.fragment_library.*
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

class LibraryFragment : BaseFragment() {

    @Inject
    lateinit var libraryAdapter: LibraryAdapter

    private lateinit var onClickLibraryItem: OnClickLibraryItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragmentComponent?.inject(this)
        onClickLibraryItem = context as OnClickLibraryItem
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_library, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        libraryAdapter.clearList()
        libraryAdapter.addAll(LibraryContent.ITEMS)
        libraryAdapter.setOnClickListener(onClickLibraryItem)

        library_list.adapter = libraryAdapter
        library_list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }

    interface OnClickLibraryItem {
        fun clickLibraryItem(position: Int, content: String, clickedItemId: Int)
    }
}