package com.gauravsaluja.mokapos.di.components

import com.gauravsaluja.mokapos.activities.ItemListActivity
import com.gauravsaluja.mokapos.base.BaseActivity
import com.gauravsaluja.mokapos.di.modules.ActivityModule
import com.gauravsaluja.mokapos.di.scope.PerActivity
import dagger.Subcomponent

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Activity component
 */

@PerActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(baseActivity: BaseActivity)

    fun inject(itemListActivity: ItemListActivity)
}