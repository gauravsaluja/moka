package com.gauravsaluja.mokapos.di.components

import com.gauravsaluja.mokapos.di.modules.FragmentModule
import com.gauravsaluja.mokapos.di.scope.PerFragment
import com.gauravsaluja.mokapos.fragments.*
import dagger.Subcomponent

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Fragment component
 */

@PerFragment
@Subcomponent(modules = arrayOf(FragmentModule::class))
interface FragmentComponent {

    fun inject(itemListFragment: ItemListFragment)

    fun inject(libraryFragment: LibraryFragment)

    fun inject(discountListFragment: DiscountListFragment)

    fun inject(shoppingCartFragment: ShoppingCartFragment)
}