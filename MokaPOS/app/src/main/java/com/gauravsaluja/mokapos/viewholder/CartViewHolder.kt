package com.gauravsaluja.mokapos.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * View holder for cart items
 */

class CartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}