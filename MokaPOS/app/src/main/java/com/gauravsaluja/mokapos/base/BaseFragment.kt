package com.gauravsaluja.mokapos.base

import android.os.Bundle
import android.support.v4.app.Fragment
import com.gauravsaluja.mokapos.application.MokaApplication
import com.gauravsaluja.mokapos.di.components.ConfigPersistentComponent
import com.gauravsaluja.mokapos.di.components.DaggerConfigPersistentComponent
import com.gauravsaluja.mokapos.di.components.FragmentComponent
import com.gauravsaluja.mokapos.di.modules.FragmentModule
import java.util.*
import java.util.concurrent.atomic.AtomicLong

/**
 * Created by Gaurav Saluja on 23-Nov-18.
 *
 * Base class for fragments
 */

abstract class BaseFragment : Fragment() {

    // get fragment component
    var fragmentComponent: FragmentComponent? = null
        private set
    private var mFragmentId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // logic to retain components on orientation change
        mFragmentId = savedInstanceState?.getLong(KEY_FRAGMENT_ID) ?: NEXT_ID.getAndIncrement()
        val configPersistentComponent: ConfigPersistentComponent?
        if (!sComponentsMap.containsKey(mFragmentId)) {
            configPersistentComponent = DaggerConfigPersistentComponent.builder()
                .appComponent((activity!!.application as MokaApplication).appComponent)
                .build()
            sComponentsMap[mFragmentId] = configPersistentComponent!!
        } else {
            configPersistentComponent = sComponentsMap[mFragmentId]
        }
        fragmentComponent = configPersistentComponent!!.fragmentComponent(FragmentModule(this))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(KEY_FRAGMENT_ID, mFragmentId)
    }

    override fun onDestroy() {
        if (!activity!!.isChangingConfigurations) {
            sComponentsMap.remove(mFragmentId)
        }
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val childFragments = childFragmentManager.fragments
        for (childFragment in childFragments) {
            childFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    companion object {

        private const val KEY_FRAGMENT_ID = "KEY_FRAGMENT_ID"
        private val sComponentsMap = HashMap<Long, ConfigPersistentComponent>()
        private val NEXT_ID = AtomicLong(0)
    }
}