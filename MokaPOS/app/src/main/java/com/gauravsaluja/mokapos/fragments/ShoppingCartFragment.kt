package com.gauravsaluja.mokapos.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gauravsaluja.data.entity.Cart
import com.gauravsaluja.data.repository.CartDbRepository
import com.gauravsaluja.data.viewmodel.CartViewModel
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.adapters.CartAdapter
import com.gauravsaluja.mokapos.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_shopping_cart.*
import kotlinx.android.synthetic.main.item_cart_discount.*
import kotlinx.android.synthetic.main.item_subtotal.*
import java.text.DecimalFormat
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

class ShoppingCartFragment : BaseFragment() {

    @Inject
    lateinit var cartAdapter: CartAdapter

    private lateinit var cartViewModel: CartViewModel
    private lateinit var cartDbRepository: CartDbRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragmentComponent?.inject(this)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_shopping_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialize()
    }

    private fun initialize() {

        cartViewModel = ViewModelProviders.of(this).get(CartViewModel::class.java)
        cartDbRepository = CartDbRepository(activity!!.application)

        cartAdapter.setOnClickListener(context!!)

        shopping_cart_list.adapter = cartAdapter
        shopping_cart_list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        cartViewModel.getAllCartItems().observe(this,
            Observer<List<Cart>> { items ->

                if (items?.size!! > 0) {
                    tag_empty_cart.visibility = View.GONE
                    shopping_cart_list.visibility = View.VISIBLE
                    subtotal_layout.visibility = View.VISIBLE
                    discount_layout.visibility = View.VISIBLE

                    cartAdapter.loadCartItems(ArrayList(items))
                    cartAdapter.notifyDataSetChanged()
                } else {
                    tag_empty_cart.visibility = View.VISIBLE
                    shopping_cart_list.visibility = View.GONE
                    subtotal_layout.visibility = View.GONE
                    discount_layout.visibility = View.GONE
                }

                setCharge(items)
            })

        action_clear_sale.setOnClickListener {
            cartDbRepository.deleteAllCartItems()
        }

        action_checkout.setOnClickListener {

        }
    }

    private fun setCharge(items: List<Cart>) {
        val amount = 0

        if (items.isNotEmpty()) {
            val subTotal = calculateSubTotal(items)
            if (Math.ceil(subTotal) == Math.floor(subTotal) && subTotal < Integer.MAX_VALUE) {
                val decimalFormat = DecimalFormat("0.#")
                value_subtotal.text = value_subtotal.context.getString(R.string.price, decimalFormat.format(subTotal))
            } else {
                value_subtotal.text = value_subtotal.context.getString(R.string.price, subTotal.toString())
            }

            val discount = calculateDiscount(items)
            if (Math.ceil(discount) == Math.floor(discount) && discount < Integer.MAX_VALUE) {
                val decimalFormat = DecimalFormat("0.#")
                value_discount.text = value_discount.context.getString(R.string.discount_price, decimalFormat.format(discount))
            } else {
                value_discount.text = value_discount.context.getString(R.string.discount_price, discount.toString())
            }

            val total = calculateSubTotal(items) - calculateDiscount(items)
            if (Math.ceil(total) == Math.floor(total) && total < Integer.MAX_VALUE) {
                val decimalFormat = DecimalFormat("0.#")
                action_checkout.text = action_checkout.context.getString(R.string.checkout_value, "$", decimalFormat.format(total))
            } else {
                action_checkout.text = action_checkout.context.getString(R.string.checkout_value, "$", String.format("%.2f", total))
            }
        } else {
            action_checkout.text = action_checkout.context.getString(R.string.checkout_value, "$", amount.toString())
        }
    }

    private fun calculateSubTotal(cartItems: List<Cart>): Double {
        var subTotal = 0.0

        for (item in cartItems) {
            subTotal += (item.itemPrice.times(item.itemQuantity))
        }

        return String.format("%.2f", subTotal).toDouble()
    }

    private fun calculateDiscount(cartItems: List<Cart>): Double {
        var discountValue = 0.0

        for (item in cartItems) {
            val discount = (item.itemPrice.times(item.itemQuantity).times(item.discountPercentage) / 100)
            discountValue += discount
        }

        return String.format("%.2f", discountValue).toDouble()
    }
}