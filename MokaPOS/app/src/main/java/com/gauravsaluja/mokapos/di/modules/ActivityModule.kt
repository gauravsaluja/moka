package com.gauravsaluja.mokapos.di.modules

import android.app.Activity
import android.content.Context
import com.gauravsaluja.mokapos.di.scope.PerActivity
import dagger.Module
import dagger.Provides

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Activity module
 */

@Module
class ActivityModule(private val mActivity: Activity) {

    @Provides
    @PerActivity
    internal fun provideActivity(): Activity {
        return mActivity
    }

    @Provides
    @PerActivity
    internal fun providesContext(): Context {
        return mActivity
    }
}