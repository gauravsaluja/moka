package com.gauravsaluja.mokapos.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gauravsaluja.data.entity.Cart
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.viewholder.CartViewHolder
import kotlinx.android.synthetic.main.cart_item_detail.view.*
import java.text.DecimalFormat
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

class CartAdapter @Inject
constructor() :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var cartItemList: ArrayList<Cart> = ArrayList()
    private lateinit var onClickCartItem: OnClickCartItem

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val viewItem = inflater.inflate(R.layout.cart_item_detail, parent, false)
        return CartViewHolder(viewItem)
    }

    fun setOnClickListener(context: Context) {
        onClickCartItem = context as OnClickCartItem
    }

    interface OnClickCartItem {
        fun clickCartItem(position: Int, item: Cart)
    }

    override fun getItemCount(): Int {
        return cartItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemLayout = holder.itemView.item_layout
        val itemName = holder.itemView.item_name
        val itemQuantity = holder.itemView.item_quantity
        val itemPrice = holder.itemView.item_price

        val item = getItem(position)

        itemName.text = itemName.context.getString(R.string.item_name, item.itemId.toString())
        itemQuantity.text = itemQuantity.context.getString(R.string.quantity_value, item.itemQuantity.toString())

        val price = item.itemPrice.times(item.itemQuantity)
        if (Math.ceil(price) == Math.floor(price) && price < Integer.MAX_VALUE) {
            val decimalFormat = DecimalFormat("0.#")
            itemPrice.text = itemPrice.context.getString(R.string.price, decimalFormat.format(price))
        } else {
            itemPrice.text = itemPrice.context.getString(R.string.price, String.format("%.2f", price))
        }

        itemLayout.setOnClickListener {
            onClickCartItem.clickCartItem(position, item)
        }
    }

    private fun getItem(position: Int): Cart {
        return cartItemList[position]
    }

    fun loadCartItems(items: ArrayList<Cart>) {
        this.cartItemList = items
    }

    fun add(cartItem: Cart) {
        cartItemList.add(cartItem)
        notifyItemInserted(cartItemList.size - 1)
    }
}