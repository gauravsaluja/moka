package com.gauravsaluja.mokapos.di.modules

import android.app.Application
import android.content.Context
import com.gauravsaluja.data.repository.ItemDataRepository
import com.gauravsaluja.domain.executor.Executor
import com.gauravsaluja.domain.executor.PostExecutionThread
import com.gauravsaluja.domain.executor.impl.ThreadExecutor
import com.gauravsaluja.domain.repository.ItemRepository
import com.gauravsaluja.mokapos.threading.MainThread
import dagger.Module
import dagger.Provides

import javax.inject.Singleton

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Application module
 */

@Module
class AppModule(internal var application: Application) {

    @Provides
    @Singleton
    internal fun providesApplication(): Application {
        return application
    }

    @Provides
    internal fun provideContext(application: Application): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    internal fun provideThreadExecutor(threadExecutor: ThreadExecutor): Executor {
        return threadExecutor
    }

    @Provides
    @Singleton
    internal fun providePostExecutionThread(uiThread: MainThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @Singleton
    internal fun provideItemRepository(itemDataRepository: ItemDataRepository): ItemRepository {
        return itemDataRepository
    }
}