package com.gauravsaluja.mokapos.threading

import android.os.Handler
import android.os.Looper
import com.gauravsaluja.domain.executor.PostExecutionThread
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Implementation of post execution thread which will provide main / UI thread
 */

@Singleton
class MainThread @Inject
constructor() : PostExecutionThread {

    private val mHandler: Handler = Handler(Looper.getMainLooper())

    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()

}