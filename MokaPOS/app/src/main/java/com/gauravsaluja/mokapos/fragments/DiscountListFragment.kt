package com.gauravsaluja.mokapos.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.adapters.DiscountListAdapter
import com.gauravsaluja.mokapos.base.BaseFragment
import com.gauravsaluja.mokapos.dummy.DiscountContent
import kotlinx.android.synthetic.main.fragment_discount_list.*
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

class DiscountListFragment : BaseFragment() {

    @Inject
    lateinit var discountListAdapter: DiscountListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragmentComponent?.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_discount_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        discountListAdapter.addAll(DiscountContent.ITEMS)

        discount_list.adapter = discountListAdapter
        discount_list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    }
}