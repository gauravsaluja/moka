package com.gauravsaluja.mokapos.dummy

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

object DiscountContent {

    val ITEMS: MutableList<DiscountItem> = ArrayList()
    val ITEM_MAP: MutableMap<Int, DiscountItem> = HashMap()
    private const val COUNT = 5

    init {
        for (i in 1..DiscountContent.COUNT) {
            DiscountContent.addItem(DiscountContent.createDiscountItem(i))
        }
    }

    private fun addItem(item: DiscountItem) {
        ITEMS.add(item)
        ITEM_MAP[item.id] = item
    }

    private fun createDiscountItem(position: Int): DiscountItem {
        return DiscountItem(position, makeDiscountNames(position), makeDiscountPercentage(position))
    }

    private fun makeDiscountNames(position: Int) : String {
        return when {
            position % COUNT == 1 -> "% Discount A"
            position % COUNT == 2 -> "% Discount B"
            position % COUNT == 3 -> "% Discount C"
            position % COUNT == 4 -> "% Discount D"
            else -> "% Discount E"
        }
    }

    private fun makeDiscountPercentage(position: Int) : Double {
        return when {
            position % COUNT == 1 -> 0.0
            position % COUNT == 2 -> 10.0
            position % COUNT == 3 -> 35.5
            position % COUNT == 4 -> 50.0
            else -> 100.0
        }
    }

    data class DiscountItem(val id: Int, val discountName: String, val discountPercent : Double) {
        override fun toString(): String = discountName
    }
}