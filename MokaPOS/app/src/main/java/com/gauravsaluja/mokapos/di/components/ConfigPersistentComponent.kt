package com.gauravsaluja.mokapos.di.components

import com.gauravsaluja.mokapos.di.modules.ActivityModule
import com.gauravsaluja.mokapos.di.modules.FragmentModule
import com.gauravsaluja.mokapos.di.scope.ConfigPersistent
import dagger.Component

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Configuration persistent component
 */

@ConfigPersistent
@Component(dependencies = arrayOf(AppComponent::class))
interface ConfigPersistentComponent {

    fun activityComponent(activityModule: ActivityModule): ActivityComponent

    fun fragmentComponent(fragmentModule: FragmentModule): FragmentComponent
}