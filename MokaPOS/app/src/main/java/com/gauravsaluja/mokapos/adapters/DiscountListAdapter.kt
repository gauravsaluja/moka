package com.gauravsaluja.mokapos.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.dummy.DiscountContent
import com.gauravsaluja.mokapos.viewholder.DiscountItemViewHolder
import kotlinx.android.synthetic.main.discount_item_detail.view.*
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

class DiscountListAdapter @Inject
constructor() :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val discountList: MutableList<DiscountContent.DiscountItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val viewItem = inflater.inflate(R.layout.discount_item_detail, parent, false)
        return DiscountItemViewHolder(viewItem)
    }

    override fun getItemCount(): Int {
        return discountList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val discountItemName = holder.itemView.discount_item_name
        val discountItemPercent = holder.itemView.discount_item_percent

        val item : DiscountContent.DiscountItem = getItem(position)

        discountItemName.text = item.discountName
        discountItemPercent.text = discountItemPercent.context.getString(R.string.discount_percentage, item.discountPercent.toString(), "%")
    }

    private fun getItem(position: Int): DiscountContent.DiscountItem {
        return discountList[position]
    }

    fun add(discountItem: DiscountContent.DiscountItem) {
        discountList.add(discountItem)
        notifyItemInserted(discountList.size - 1)
    }

    fun addAll(discountList: List<DiscountContent.DiscountItem>) {
        for (product in discountList) {
            add(product)
        }
    }
}