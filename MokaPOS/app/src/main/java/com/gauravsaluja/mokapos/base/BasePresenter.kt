package com.gauravsaluja.mokapos.base

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Base class for presenters
 */

interface BasePresenter {

    fun resume()

    fun pause()

    fun stop()

    fun destroy()
}