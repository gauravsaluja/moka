package com.gauravsaluja.mokapos.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.SwitchCompat
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.CompoundButton
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import com.gauravsaluja.data.entity.Cart
import com.gauravsaluja.data.entity.Item
import com.gauravsaluja.data.repository.CartDbRepository
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.adapters.CartAdapter
import com.gauravsaluja.mokapos.adapters.ItemListAdapter
import com.gauravsaluja.mokapos.dummy.DiscountContent
import com.gauravsaluja.mokapos.fragments.DiscountListFragment
import com.gauravsaluja.mokapos.fragments.ItemListFragment
import com.gauravsaluja.mokapos.fragments.LibraryFragment
import com.gauravsaluja.mokapos.fragments.ShoppingCartFragment
import kotlinx.android.synthetic.main.activity_item_list.*
import kotlinx.android.synthetic.main.item_list.*

class ItemListActivity : AppCompatActivity(), LibraryFragment.OnClickLibraryItem, ItemListAdapter.OnClickItem,
    CartAdapter.OnClickCartItem {

    private lateinit var popup: PopupWindow

    private lateinit var itemDetails: TextView
    private lateinit var valueQuantity: TextView
    private lateinit var increaseQuantity: AppCompatImageView
    private lateinit var decreaseQuantity: AppCompatImageView
    private lateinit var actionCancel: TextView
    private lateinit var actionSave: TextView

    private lateinit var switchDiscount1: SwitchCompat
    private lateinit var switchDiscount2: SwitchCompat
    private lateinit var switchDiscount3: SwitchCompat
    private lateinit var switchDiscount4: SwitchCompat
    private lateinit var switchDiscount5: SwitchCompat

    private var quantity = 0

    override fun clickCartItem(position: Int, item: Cart) {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        val point = Point()
        if (twoPane) {
            point.set(width / 4, height / 4)
        } else {
            point.set(width / 10, height / 10)
        }

        showPopup(this, point, width, height, item)
    }

    override fun clickItem(position: Int, item: Item) {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        val point = Point()

        if (twoPane) {
            point.set(width / 4, height / 4)
        } else {
            point.set(width / 10, height / 10)
        }

        showPopup(this, point, width, height, item)
    }

    private fun popupBase(context: Activity, p: Point, width: Int, height: Int) {

        var popupWidth = 0
        var popupHeight = 0

        if (twoPane) {
            popupWidth = width / 2
            popupHeight = height / 2
        } else {
            popupWidth = (width / 1.2).toInt()
            popupHeight = (height / 1.2).toInt()
        }

        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = layoutInflater.inflate(R.layout.popup_layout, null)
        popup = PopupWindow(context)
        popup.contentView = layout
        popup.width = popupWidth
        popup.height = popupHeight
        popup.isFocusable = true
        popup.setBackgroundDrawable(BitmapDrawable())
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x, p.y)

        itemDetails = layout.findViewById(R.id.item_details) as TextView
        valueQuantity = layout.findViewById(R.id.value_quantity) as TextView
        increaseQuantity = layout.findViewById(R.id.increase_quantity) as AppCompatImageView
        decreaseQuantity = layout.findViewById(R.id.decrease_quantity) as AppCompatImageView
        actionCancel = layout.findViewById(R.id.action_cancel) as TextView
        actionSave = layout.findViewById(R.id.action_save) as TextView

        switchDiscount1 = layout.findViewById(R.id.switch_discount_1) as SwitchCompat
        switchDiscount2 = layout.findViewById(R.id.switch_discount_2) as SwitchCompat
        switchDiscount3 = layout.findViewById(R.id.switch_discount_3) as SwitchCompat
        switchDiscount4 = layout.findViewById(R.id.switch_discount_4) as SwitchCompat
        switchDiscount5 = layout.findViewById(R.id.switch_discount_5) as SwitchCompat

        actionCancel.setOnClickListener {
            popup.dismiss()
        }

        switchDiscount1.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        switchDiscount2.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        switchDiscount3.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        switchDiscount4.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        switchDiscount5.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        increaseQuantity.setOnClickListener {
            if (quantity < 1000) {
                quantity += 1
                valueQuantity.text = quantity.toString()
            }
        }

        decreaseQuantity.setOnClickListener {
            if (quantity > 0) {
                quantity -= 1
                valueQuantity.text = quantity.toString()
            }
        }
    }

    private fun makeSwitchesNotClickable() {
        switchDiscount1.isClickable = false
        switchDiscount2.isClickable = false
        switchDiscount3.isClickable = false
        switchDiscount4.isClickable = false
        switchDiscount5.isClickable = false
    }

    private fun makeSwitchesClickable() {
        switchDiscount1.isClickable = true
        switchDiscount2.isClickable = true
        switchDiscount3.isClickable = true
        switchDiscount4.isClickable = true
        switchDiscount5.isClickable = true
    }

    private fun showPopup(context: Activity, p: Point, width: Int, height: Int, item: Cart) {
        popupBase(context, p, width, height)

        quantity = item.itemQuantity

        itemDetails.text = item.itemName
        valueQuantity.text = quantity.toString()

        makeSwitchesNotClickable()

        when {
            item.discountId == 1 -> switchDiscount1.isChecked = true
            item.discountId == 2 -> switchDiscount2.isChecked = true
            item.discountId == 3 -> switchDiscount3.isChecked = true
            item.discountId == 4 -> switchDiscount4.isChecked = true
            item.discountId == 5 -> switchDiscount5.isChecked = true
        }

        actionSave.setOnClickListener {
            quantity = valueQuantity.text.toString().toInt()

            val cartDbRepository = CartDbRepository(application)

            if (quantity > 0) {
                item.itemQuantity = quantity
                item.discountId = item.discountId
                item.discountPercentage = item.discountPercentage

                cartDbRepository.update(cart = item)
            } else {
                cartDbRepository.deleteItem(cart = item)
            }

            popup.dismiss()
        }
    }

    private fun showPopup(context: Activity, p: Point, width: Int, height: Int, item: Item) {

        popupBase(context, p, width, height)

        quantity = 0

        itemDetails.text = getString(R.string.item_name, item.id.toString())
        valueQuantity.text = quantity.toString()

        makeSwitchesClickable()

        actionSave.setOnClickListener {
            quantity = valueQuantity.text.toString().toInt()

            if (quantity > 0) {
                val selectedDiscountId = getSelectedDiscount()
                val discountItem = DiscountContent.ITEM_MAP[selectedDiscountId]

                val cartDbRepository = CartDbRepository(application)
                val cartItem = cartDbRepository.getCartItemByItemId(itemId = item.id!!)

                if (cartItem != null) {
                    if (cartItem.discountId == discountItem?.id) {
                        cartItem.itemQuantity = cartItem.itemQuantity + quantity
                        cartDbRepository.update(cartItem)
                    } else {
                        val addItem = Cart(
                            item.id!!,
                            getString(R.string.item_name, item.id.toString()),
                            item.price!!,
                            quantity,
                            discountItem!!.id,
                            discountItem.discountPercent
                        )
                        cartDbRepository.insert(addItem)
                    }
                } else {
                    val addItem = Cart(
                        item.id!!,
                        getString(R.string.item_name, item.id.toString()),
                        item.price!!,
                        quantity,
                        discountItem!!.id,
                        discountItem.discountPercent
                    )
                    cartDbRepository.insert(addItem)
                }

                popup.dismiss()
            } else {
                Toast.makeText(this, "Please add a quantity", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun getSelectedDiscount(): Int {
        if (switchDiscount1.isChecked)
            return 1

        if (switchDiscount2.isChecked)
            return 2

        if (switchDiscount3.isChecked)
            return 3

        if (switchDiscount4.isChecked)
            return 4

        if (switchDiscount5.isChecked)
            return 5

        return 1
    }

    private fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            if (buttonView !== switchDiscount1) {
                switchDiscount1.isChecked = false
            }
            if (buttonView !== switchDiscount2) {
                switchDiscount2.isChecked = false
            }
            if (buttonView !== switchDiscount3) {
                switchDiscount3.isChecked = false
            }
            if (buttonView !== switchDiscount4) {
                switchDiscount4.isChecked = false
            }
            if (buttonView !== switchDiscount5) {
                switchDiscount5.isChecked = false
            }
        }
    }

    override fun clickLibraryItem(position: Int, content: String, clickedItemId: Int) {
        if (position == 0) {
            replaceWithDiscountListFragment()
        } else {
            replaceWithItemListFragment()
        }
    }

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (item_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        setupItemContainer()
    }

    private fun replaceWithDiscountListFragment() {
        val fragment = DiscountListFragment().apply {
            arguments = Bundle().apply {

            }
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.item_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun replaceWithItemListFragment() {
        val fragment = ItemListFragment().apply {
            arguments = Bundle().apply {

            }
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.item_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun setupItemContainer() {
        val fragment = LibraryFragment().apply {
            arguments = Bundle().apply {

            }
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.item_container, fragment)
            .commit()

        if (twoPane) {
            action_shopping_cart.hide()

            val fragment1 = ShoppingCartFragment().apply {
                arguments = Bundle().apply {

                }
            }

            supportFragmentManager
                .beginTransaction()
                .replace(R.id.item_detail_container, fragment1)
                .commit()
        } else {
            action_shopping_cart.show()

            action_shopping_cart.setOnClickListener {
                val intent = Intent(this, ShoppingCartActivity::class.java).apply {
                    //putExtra(ItemDetailFragment.ARG_ITEM_ID, item.id)
                }

                startActivity(intent)
            }
        }
    }
}
