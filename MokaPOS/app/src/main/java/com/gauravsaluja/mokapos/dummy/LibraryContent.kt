package com.gauravsaluja.mokapos.dummy

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

object LibraryContent {

    val ITEMS: MutableList<LibraryItem> = ArrayList()
    private val ITEM_MAP: MutableMap<Int, LibraryItem> = HashMap()
    private const val COUNT = 2

    init {
        for (i in 1..LibraryContent.COUNT) {
            LibraryContent.addItem(LibraryContent.createLibraryItem(i))
        }
    }

    private fun addItem(item: LibraryItem) {
        ITEMS.add(item)
        ITEM_MAP[item.id] = item
    }

    private fun createLibraryItem(position: Int): LibraryItem {
        return LibraryItem(position, makeDetails(position))
    }

    private fun makeDetails(position: Int): String {
        return when {
            position % COUNT == 1 -> "All Discounts"
            else -> "All Items"
        }
    }

    data class LibraryItem(val id: Int, val content: String) {
        override fun toString(): String = content
    }
}