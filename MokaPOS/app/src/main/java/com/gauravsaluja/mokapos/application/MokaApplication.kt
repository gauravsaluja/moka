package com.gauravsaluja.mokapos.application

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import com.gauravsaluja.domain.utils.Constants
import com.gauravsaluja.mokapos.di.components.AppComponent
import com.gauravsaluja.mokapos.di.components.DaggerAppComponent
import com.gauravsaluja.mokapos.di.modules.AppModule
import com.gauravsaluja.mokapos.di.modules.NetworkModule

/**
 * Created by Gaurav Saluja on 23-Nov-18.
 *
 * Application class
 */

class MokaApplication : Application() {

    // get app component
    val appComponent: AppComponent
        get() {
            if (sAppComponent == null)
                synchronized(sObjectLock) {
                    if (sAppComponent == null) {
                        sAppComponent = DaggerAppComponent.builder()
                            .appModule(applicationModule)
                            .networkModule(networkModule)
                            .build()
                    }
                }
            return sAppComponent!!
        }

    // get application module
    private val applicationModule: AppModule
        get() = AppModule(this)

    // get network module
    private val networkModule: NetworkModule
        get() = NetworkModule(Constants.BASE_URL)

    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    companion object {

        private var sAppComponent: AppComponent? = null
        private val sObjectLock = Any()
    }
}