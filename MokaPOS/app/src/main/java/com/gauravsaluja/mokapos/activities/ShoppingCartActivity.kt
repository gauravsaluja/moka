package com.gauravsaluja.mokapos.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.SwitchCompat
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.CompoundButton
import android.widget.PopupWindow
import android.widget.TextView
import com.gauravsaluja.data.entity.Cart
import com.gauravsaluja.data.repository.CartDbRepository
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.adapters.CartAdapter
import com.gauravsaluja.mokapos.fragments.ShoppingCartFragment
import kotlinx.android.synthetic.main.activity_shopping_cart.*

class ShoppingCartActivity : AppCompatActivity(), CartAdapter.OnClickCartItem {

    private lateinit var popup: PopupWindow

    private lateinit var itemDetails: TextView
    private lateinit var valueQuantity: TextView
    private lateinit var increaseQuantity: AppCompatImageView
    private lateinit var decreaseQuantity: AppCompatImageView
    private lateinit var actionCancel: TextView
    private lateinit var actionSave: TextView

    private lateinit var switchDiscount1: SwitchCompat
    private lateinit var switchDiscount2: SwitchCompat
    private lateinit var switchDiscount3: SwitchCompat
    private lateinit var switchDiscount4: SwitchCompat
    private lateinit var switchDiscount5: SwitchCompat

    private var quantity = 0

    override fun clickCartItem(position: Int, item: Cart) {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels

        val point = Point()
        point.set(width / 10, height / 10)

        showPopup(this, point, width, height, item)
    }

    private fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            if (buttonView !== switchDiscount1) {
                switchDiscount1.isChecked = false
            }
            if (buttonView !== switchDiscount2) {
                switchDiscount2.isChecked = false
            }
            if (buttonView !== switchDiscount3) {
                switchDiscount3.isChecked = false
            }
            if (buttonView !== switchDiscount4) {
                switchDiscount4.isChecked = false
            }
            if (buttonView !== switchDiscount5) {
                switchDiscount5.isChecked = false
            }
        }
    }

    private fun popupBase(context: Activity, p: Point, width: Int, height: Int) {

        var popupWidth = 0
        var popupHeight = 0

        popupWidth = (width / 1.2).toInt()
        popupHeight = (height / 1.2).toInt()

        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout = layoutInflater.inflate(R.layout.popup_layout, null)
        popup = PopupWindow(context)
        popup.contentView = layout
        popup.width = popupWidth
        popup.height = popupHeight
        popup.isFocusable = true
        popup.setBackgroundDrawable(BitmapDrawable())
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x, p.y)

        itemDetails = layout.findViewById(R.id.item_details) as TextView
        valueQuantity = layout.findViewById(R.id.value_quantity) as TextView
        increaseQuantity = layout.findViewById(R.id.increase_quantity) as AppCompatImageView
        decreaseQuantity = layout.findViewById(R.id.decrease_quantity) as AppCompatImageView
        actionCancel = layout.findViewById(R.id.action_cancel) as TextView
        actionSave = layout.findViewById(R.id.action_save) as TextView

        switchDiscount1 = layout.findViewById(R.id.switch_discount_1) as SwitchCompat
        switchDiscount2 = layout.findViewById(R.id.switch_discount_2) as SwitchCompat
        switchDiscount3 = layout.findViewById(R.id.switch_discount_3) as SwitchCompat
        switchDiscount4 = layout.findViewById(R.id.switch_discount_4) as SwitchCompat
        switchDiscount5 = layout.findViewById(R.id.switch_discount_5) as SwitchCompat

        actionCancel.setOnClickListener {
            popup.dismiss()
        }

        switchDiscount1.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        switchDiscount2.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        switchDiscount3.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        switchDiscount4.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        switchDiscount5.setOnCheckedChangeListener { buttonView, isChecked ->
            onCheckedChanged(buttonView, isChecked)
        }

        increaseQuantity.setOnClickListener {
            if (quantity < 1000) {
                quantity += 1
                valueQuantity.text = quantity.toString()
            }
        }

        decreaseQuantity.setOnClickListener {
            if (quantity > 0) {
                quantity -= 1
                valueQuantity.text = quantity.toString()
            }
        }
    }

    private fun makeSwitchesNotClickable() {
        switchDiscount1.isClickable = false
        switchDiscount2.isClickable = false
        switchDiscount3.isClickable = false
        switchDiscount4.isClickable = false
        switchDiscount5.isClickable = false
    }

    private fun showPopup(context: Activity, p: Point, width: Int, height: Int, item: Cart) {
        popupBase(context, p, width, height)

        quantity = item.itemQuantity

        itemDetails.text = item.itemName
        valueQuantity.text = quantity.toString()

        makeSwitchesNotClickable()

        when {
            item.discountId == 1 -> switchDiscount1.isChecked = true
            item.discountId == 2 -> switchDiscount2.isChecked = true
            item.discountId == 3 -> switchDiscount3.isChecked = true
            item.discountId == 4 -> switchDiscount4.isChecked = true
            item.discountId == 5 -> switchDiscount5.isChecked = true
        }

        actionSave.setOnClickListener {
            quantity = valueQuantity.text.toString().toInt()

            val cartDbRepository = CartDbRepository(application)

            if (quantity > 0) {
                item.itemQuantity = quantity
                item.discountId = item.discountId
                item.discountPercentage = item.discountPercentage

                cartDbRepository.update(cart = item)
            } else {
                cartDbRepository.deleteItem(cart = item)
            }

            popup.dismiss()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_cart)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            // Create the shopping cart fragment and add it to the activity
            // using a fragment transaction.

            val fragment = ShoppingCartFragment().apply {
                arguments = Bundle().apply {

                }
            }

            supportFragmentManager.beginTransaction()
                .add(R.id.frameLayout, fragment)
                .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpTo(this, Intent(this, ItemListActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}
