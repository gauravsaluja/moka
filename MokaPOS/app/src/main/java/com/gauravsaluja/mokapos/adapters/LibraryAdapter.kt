package com.gauravsaluja.mokapos.adapters

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.dummy.LibraryContent
import com.gauravsaluja.mokapos.fragments.LibraryFragment
import com.gauravsaluja.mokapos.viewholder.LibraryItemViewHolder
import kotlinx.android.synthetic.main.item_detail.view.*
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

class LibraryAdapter @Inject
constructor() :
    RecyclerView.Adapter<ViewHolder>() {

    private val libraryList: MutableList<LibraryContent.LibraryItem> = ArrayList()
    private lateinit var onClickLibraryItem: LibraryFragment.OnClickLibraryItem

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val viewItem = inflater.inflate(R.layout.item_detail, parent, false)
        return LibraryItemViewHolder(viewItem)
    }

    override fun getItemCount(): Int {
        return libraryList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val itemDetail = holder.itemView.item_detail
        itemDetail.text = getItem(position).content

        itemDetail.setOnClickListener {
            onClickLibraryItem.clickLibraryItem(position, getItem(position).content, getItem(position).id)
        }
    }

    private fun getItem(position: Int): LibraryContent.LibraryItem {
        return libraryList[position]
    }

    fun setOnClickListener(onClickItem: LibraryFragment.OnClickLibraryItem) {
        this.onClickLibraryItem = onClickItem
    }

    fun add(libraryItem: LibraryContent.LibraryItem) {
        libraryList.add(libraryItem)
        notifyItemInserted(libraryList.size - 1)
    }

    fun addAll(libraryList: List<LibraryContent.LibraryItem>) {
        for (product in libraryList) {
            add(product)
        }
    }

    fun clearList() {
        libraryList.clear()
    }
}