package com.gauravsaluja.mokapos.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.gauravsaluja.data.entity.Item
import com.gauravsaluja.data.repository.ItemDbRepository
import com.gauravsaluja.data.viewmodel.ItemViewModel
import com.gauravsaluja.domain.model.ItemData
import com.gauravsaluja.mokapos.R
import com.gauravsaluja.mokapos.adapters.ItemListAdapter
import com.gauravsaluja.mokapos.base.BaseFragment
import com.gauravsaluja.mokapos.presenters.ItemsPresenter
import com.gauravsaluja.mokapos.presenters.impl.ItemsPresenterImpl
import com.gauravsaluja.mokapos.utils.Connectivity
import kotlinx.android.synthetic.main.fragment_item_list.*
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 */

class ItemListFragment : BaseFragment(), ItemsPresenter.View {

    @Inject
    lateinit var itemsPresenter: ItemsPresenterImpl
    @Inject
    lateinit var itemListAdapter: ItemListAdapter

    private lateinit var itemViewModel: ItemViewModel
    private lateinit var itemDbRepository: ItemDbRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragmentComponent?.inject(this)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        itemsPresenter.setView(this)
        initialize()
    }

    private fun initialize() {

        itemListAdapter.setOnClickListener(context!!)

        item_list.adapter = itemListAdapter
        item_list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        itemViewModel = ViewModelProviders.of(this).get(ItemViewModel::class.java)
        itemDbRepository = ItemDbRepository(activity!!.application)
        val noOfItems = itemDbRepository.getNoOfItems()

        if (noOfItems < 5000) {
            loadItemList()
        } else {
            itemViewModel.getAllItems().observe(this,
                Observer<List<Item>> { items ->
                    itemListAdapter.addAll(items!!)
                })
        }
    }

    private fun loadItemList() {
        if (Connectivity.isConnected(context!!)) {
            try {
                itemsPresenter.load()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {

        }
    }

    override fun onItemsLoading() {
        Toast.makeText(context, "Loading items from network", Toast.LENGTH_SHORT).show()
    }

    override fun onItemsLoaded(itemDataResponse: List<ItemData>) {
        itemDbRepository.deleteAllItems()

        val itemList: ArrayList<Item> = ArrayList()

        for (itemData in itemDataResponse) {
            val rand = kotlin.random.Random
            val randomNo: Int = rand.nextInt(10, 99)
            val price = (randomNo * itemData.id!!).toDouble()

            val item = Item(itemData.id, itemData.albumId, itemData.title!!, itemData.url!!, itemData.thumbnailUrl!!, price)
            itemList.add(item)
            itemDbRepository.insert(item)
        }

        item_list.visibility = View.VISIBLE
        itemListAdapter.addAll(itemList)
    }

    override fun onItemsFailed() {

    }

    override fun onDestroy() {
        super.onDestroy()

        itemsPresenter.destroy()
    }
}