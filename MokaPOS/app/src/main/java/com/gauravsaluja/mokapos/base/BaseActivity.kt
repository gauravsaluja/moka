package com.gauravsaluja.mokapos.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gauravsaluja.mokapos.application.MokaApplication
import com.gauravsaluja.mokapos.di.components.ActivityComponent
import com.gauravsaluja.mokapos.di.components.ConfigPersistentComponent
import com.gauravsaluja.mokapos.di.components.DaggerConfigPersistentComponent
import com.gauravsaluja.mokapos.di.modules.ActivityModule
import java.util.*
import java.util.concurrent.atomic.AtomicLong

/**
 * Created by Gaurav Saluja on 23-Nov-18.
 *
 *
 * Base class for activities
 */

abstract class BaseActivity : AppCompatActivity() {

    // get activity component
    private var activityComponent: ActivityComponent? = null
    private var mActivityId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // logic to retain components on orientation change
        mActivityId = savedInstanceState?.getLong(KEY_ACTIVITY_ID) ?: NEXT_ID.getAndIncrement()
        val configPersistentComponent: ConfigPersistentComponent?
        if (!sComponentsMap.containsKey(mActivityId)) {
            configPersistentComponent = DaggerConfigPersistentComponent.builder()
                .appComponent((application as MokaApplication).appComponent)
                .build()
            sComponentsMap[mActivityId] = configPersistentComponent!!
        } else {
            configPersistentComponent = sComponentsMap[mActivityId]
        }
        activityComponent = configPersistentComponent!!.activityComponent(ActivityModule(this))
        activityComponent!!.inject(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(KEY_ACTIVITY_ID, mActivityId)
    }

    override fun onDestroy() {
        if (!isChangingConfigurations) {
            sComponentsMap.remove(mActivityId)
        }
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        val childFragments = supportFragmentManager.fragments
        for (childFragment in childFragments) {
            childFragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    companion object {

        private const val KEY_ACTIVITY_ID = "KEY_ACTIVITY_ID"
        private val NEXT_ID = AtomicLong(0)
        private val sComponentsMap = HashMap<Long, ConfigPersistentComponent>()
    }
}