package com.gauravsaluja.mokapos.customviews

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.gauravsaluja.mokapos.utils.Helpers

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Custom textview of typeface Nunito Light font
 */

class TextViewNunitoLightFont : AppCompatTextView {

    constructor(context: Context) : super(context) {
        setCustomFont(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        setCustomFont(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        setCustomFont(context)
    }

    // set custom font
    private fun setCustomFont(context: Context?) {
        if (context != null && !isInEditMode) {
            typeface = Helpers.getNunitoTypeFaceLight(context)
        }
    }
}