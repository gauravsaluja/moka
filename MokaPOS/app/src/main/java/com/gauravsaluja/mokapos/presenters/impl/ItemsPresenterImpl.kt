package com.gauravsaluja.mokapos.presenters.impl

import com.gauravsaluja.domain.interactors.GetItemListUseCase
import com.gauravsaluja.domain.interactors.UseCaseObserver
import com.gauravsaluja.domain.model.ItemData
import com.gauravsaluja.mokapos.presenters.ItemsPresenter
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Implementation of item listing contract
 */

class ItemsPresenterImpl @Inject
constructor(private val itemListUseCase: GetItemListUseCase) : ItemsPresenter {
    private var presenterView: ItemsPresenter.View? = null

    // observer for the network call
    private val itemDataObserver: UseCaseObserver<List<ItemData>>
        get() = object : UseCaseObserver<List<ItemData>>() {
            override fun onNext(t: List<ItemData>) {
                super.onNext(t)
                presenterView!!.onItemsLoaded(t)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                e.printStackTrace()
                presenterView!!.onItemsFailed()
            }
        }

    override fun setView(view: ItemsPresenter.View) {
        this.presenterView = view
    }

    @Throws(Exception::class)
    override fun load() {

        // if view is set then process with request
        // else throw exception
        if (presenterView != null) {
            presenterView!!.onItemsLoading()
        } else {
            throw Exception("setView() not called Before calling load()")
        }

        // dispose existing instance of use case (if any)
        itemListUseCase.dispose()

        // send the load request
        itemListUseCase.execute(itemDataObserver)
    }

    override fun resume() {

    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {
        presenterView = null
        itemListUseCase.dispose()
    }
}