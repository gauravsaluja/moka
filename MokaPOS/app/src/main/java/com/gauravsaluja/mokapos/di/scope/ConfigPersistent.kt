package com.gauravsaluja.mokapos.di.scope

import javax.inject.Scope

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Configuration persistent scope
 */

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ConfigPersistent