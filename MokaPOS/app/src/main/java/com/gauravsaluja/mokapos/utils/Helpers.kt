package com.gauravsaluja.mokapos.utils

import android.content.Context
import android.graphics.Typeface

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Helper class
 */

object Helpers {

    // get typeface nunito bold
    fun getNunitoTypeFaceBold(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "fonts/nunito_bold.ttf")
    }

    // get typeface nunito regular
    fun getNunitoTypeFaceRegular(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "fonts/nunito_regular.ttf")
    }

    // get typeface nunito light
    fun getNunitoTypeFaceLight(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "fonts/nunito_light.ttf")
    }
}