package com.gauravsaluja.data.repository

import com.gauravsaluja.data.api.ItemsApi
import com.gauravsaluja.domain.model.ItemData
import com.gauravsaluja.domain.repository.ItemRepository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Implementation of repository class defined in 'domain' module
 */

class ItemDataRepository @Inject
constructor(private val itemsApi: ItemsApi) : ItemRepository {

    // get item data
    override fun getItemsList(): Observable<List<ItemData>> {
        return itemsApi.getItemData()
    }
}