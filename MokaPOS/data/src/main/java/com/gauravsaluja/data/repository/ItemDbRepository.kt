package com.gauravsaluja.data.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import com.gauravsaluja.data.dao.ItemDao
import com.gauravsaluja.data.database.ItemRoomDatabase
import com.gauravsaluja.data.entity.Item

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Repository for item_data
 */

class ItemDbRepository(application: Application) {

    private var itemDao: ItemDao? = null
    var allItemsDb: LiveData<List<Item>>? = null
        private set

    init {
        val db = ItemRoomDatabase.getDatabase(application)
        if (db != null) {
            itemDao = db.itemsDao()
            allItemsDb = itemDao!!.allItems
        }
    }

    fun insert(item: Item) {
        itemDao?.let {
            InsertAsyncTask(it).execute(item)
        }
    }

    fun getNoOfItems() : Int {
        itemDao?.let {
            return GetItemsAsyncTask(it).execute().get()
        }
    }

    fun deleteAllItems() {
        itemDao?.let {
            DeleteAllAsyncTask(it).execute()
        }
    }

    private class InsertAsyncTask (private val mAsyncTaskDao: ItemDao) :
        AsyncTask<Item, Void, Void>() {

        override fun doInBackground(vararg params: Item): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }

    private class GetItemsAsyncTask (private val mAsyncTaskDao: ItemDao) :
        AsyncTask<Void, Void, Int>() {

        override fun doInBackground(vararg params: Void): Int? {
            return mAsyncTaskDao.getNumberOfItems()
        }
    }

    private class DeleteAllAsyncTask (private val mAsyncTaskDao: ItemDao) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void): Void? {
            mAsyncTaskDao.deleteAll()
            return null
        }
    }
}