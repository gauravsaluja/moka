package com.gauravsaluja.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.gauravsaluja.data.dao.CartDao
import com.gauravsaluja.data.dao.ItemDao
import com.gauravsaluja.data.entity.Cart
import com.gauravsaluja.data.entity.Item

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Database class for item_data and cart table
 */

@Database(entities = [Item::class, Cart::class], version = 1)
abstract class ItemRoomDatabase : RoomDatabase() {

    abstract fun itemsDao(): ItemDao

    abstract fun cartDao(): CartDao

    companion object {

        @Volatile
        private var INSTANCE: ItemRoomDatabase? = null

        fun getDatabase(context: Context): ItemRoomDatabase? {
            if (INSTANCE == null) {
                synchronized(ItemRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            ItemRoomDatabase::class.java, "app_db"
                        ).build()
                    }
                }
            }
            return INSTANCE
        }
    }
}
