package com.gauravsaluja.data.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.gauravsaluja.data.entity.Cart

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Dao for cart table
 */

@Dao
interface CartDao {

    @get:Query("SELECT * from cart")
    val allCartItems: LiveData<List<Cart>>

    @Query("SELECT * from cart")
    fun getCartItems() : List<Cart>

    @Query("SELECT * FROM cart WHERE item_id = :itemId")
    fun getCartItemByItemId(itemId: Int): Cart?

    @Query("SELECT * FROM cart WHERE cartId = :cartId")
    fun getCartItemByCartId(cartId: Int): Cart?

    @Insert
    fun insert(cart: Cart)

    @Update
    fun updateCartItem(cart: Cart)

    @Query("DELETE FROM cart")
    fun deleteAll()

    @Delete
    fun deleteItem(cart: Cart)

    @Query("SELECT COUNT(item_id) FROM cart")
    fun getNumberOfCartItems(): Int
}