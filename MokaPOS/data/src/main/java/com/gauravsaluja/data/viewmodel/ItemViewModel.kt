package com.gauravsaluja.data.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.gauravsaluja.data.entity.Item
import com.gauravsaluja.data.repository.ItemDbRepository

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * View mode for items
 */

class ItemViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository: ItemDbRepository = ItemDbRepository(application)
    private val allItems: LiveData<List<Item>>?

    init {
        allItems = mRepository.allItemsDb
    }

    fun getAllItems() : LiveData<List<Item>> {
        return allItems!!
    }
}