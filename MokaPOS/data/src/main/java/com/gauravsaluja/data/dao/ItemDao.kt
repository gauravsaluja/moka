package com.gauravsaluja.data.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.gauravsaluja.data.entity.Item

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Dao for item_data table
 */

@Dao
interface ItemDao {

    @get:Query("SELECT * from item_data")
    val allItems: LiveData<List<Item>>

    @Insert
    fun insert(item: Item)

    @Query("DELETE FROM item_data")
    fun deleteAll()

    @Query("SELECT COUNT(id) FROM item_data")
    fun getNumberOfItems(): Int
}