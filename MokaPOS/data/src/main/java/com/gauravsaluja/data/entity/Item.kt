package com.gauravsaluja.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Entity class
 */

@Entity(tableName = "item_data")
class Item(

    @field:PrimaryKey
    @field:ColumnInfo(name = "id")
    var id: Int?,

    @field:ColumnInfo(name = "album_id")
    var albumId: Int?,

    @field:ColumnInfo(name = "title")
    var title: String,

    @field:ColumnInfo(name = "url")
    var url: String,

    @field:ColumnInfo(name = "thumbnail_url")
    var thumbnailUrl: String,

    @field:ColumnInfo(name = "price")
    var price: Double?
)