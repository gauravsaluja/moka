package com.gauravsaluja.data.api

import com.gauravsaluja.domain.model.ItemData
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Interface for defining the API calls available with params and request types
 */

interface ItemsApi {

    @GET("photos")
    fun getItemData(): Observable<List<ItemData>>
}