package com.gauravsaluja.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Entity class
 */

@Entity(tableName = "cart")
class Cart(

    @field:ColumnInfo(name = "item_id")
    var itemId: Int,

    @field:ColumnInfo(name = "item_name")
    var itemName: String,

    @field:ColumnInfo(name = "item_price")
    var itemPrice: Double,

    @field:ColumnInfo(name = "item_quantity")
    var itemQuantity: Int,

    @field:ColumnInfo(name = "discount_id")
    var discountId: Int,

    @field:ColumnInfo(name = "discount_percentage")
    var discountPercentage: Double
) {
    @PrimaryKey(autoGenerate = true)
    var cartId: Int = 0
}