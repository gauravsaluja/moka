package com.gauravsaluja.data.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import com.gauravsaluja.data.dao.CartDao
import com.gauravsaluja.data.database.ItemRoomDatabase
import com.gauravsaluja.data.entity.Cart

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * Repository for cart
 */

class CartDbRepository(application: Application) {

    private var cartDao: CartDao? = null
    lateinit var allCartItemsDb: LiveData<List<Cart>>
        private set

    init {
        val db = ItemRoomDatabase.getDatabase(application)
        if (db != null) {
            cartDao = db.cartDao()
            allCartItemsDb = cartDao!!.allCartItems
        }
    }

    fun insert(cart: Cart) {
        cartDao?.let {
            InsertAsyncTask(it).execute(cart)
        }
    }

    fun update(cart: Cart) {
        cartDao?.let {
            UpdateAsyncTask(it).execute(cart)
        }
    }

    fun getAllCartItems(): List<Cart> {
        cartDao?.let {
            return GetAllItemsAsyncTask(it).execute().get()
        }
    }

    fun getCartItemByItemId(itemId: Int): Cart? {
        cartDao?.let {
            return GetCartItemByItemIdAsyncTask(it).execute(itemId).get()
        }
    }

    fun getCartItemByCartId(itemId: Int): Cart? {
        cartDao?.let {
            return GetCartItemByCartIdAsyncTask(it).execute(itemId).get()
        }
    }

    fun getNoOfCartItems(): Int {
        cartDao?.let {
            return GetItemsAsyncTask(it).execute().get()
        }
    }

    fun deleteAllCartItems() {
        cartDao?.let {
            DeleteAllAsyncTask(it).execute()
        }
    }

    fun deleteItem(cart: Cart) {
        cartDao?.let {
            DeleteItemAsyncTask(it).execute(cart)
        }
    }

    private class InsertAsyncTask(private val mAsyncTaskDao: CartDao) :
        AsyncTask<Cart, Void, Void>() {

        override fun doInBackground(vararg params: Cart): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }

    private class UpdateAsyncTask(private val mAsyncTaskDao: CartDao) :
        AsyncTask<Cart, Void, Void>() {

        override fun doInBackground(vararg params: Cart): Void? {
            mAsyncTaskDao.updateCartItem(params[0])
            return null
        }
    }

    private class GetAllItemsAsyncTask(private val mAsyncTaskDao: CartDao) :
        AsyncTask<Void, Void, List<Cart>>() {

        override fun doInBackground(vararg params: Void): List<Cart>? {
            return mAsyncTaskDao.getCartItems()
        }
    }

    private class GetCartItemByItemIdAsyncTask(private val mAsyncTaskDao: CartDao) :
        AsyncTask<Int, Void, Cart?>() {

        override fun doInBackground(vararg params: Int?): Cart? {
            return if (params.isNotEmpty()) {
                mAsyncTaskDao.getCartItemByItemId(params[0]!!)
            } else {
                null
            }
        }
    }

    private class GetCartItemByCartIdAsyncTask(private val mAsyncTaskDao: CartDao) :
        AsyncTask<Int, Void, Cart?>() {

        override fun doInBackground(vararg params: Int?): Cart? {
            return if (params.isNotEmpty()) {
                mAsyncTaskDao.getCartItemByCartId(params[0]!!)
            } else {
                null
            }
        }
    }

    private class GetItemsAsyncTask(private val mAsyncTaskDao: CartDao) :
        AsyncTask<Void, Void, Int>() {

        override fun doInBackground(vararg params: Void): Int? {
            return mAsyncTaskDao.getNumberOfCartItems()
        }
    }

    private class DeleteAllAsyncTask(private val mAsyncTaskDao: CartDao) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void): Void? {
            mAsyncTaskDao.deleteAll()
            return null
        }
    }

    private class DeleteItemAsyncTask(private val mAsyncTaskDao: CartDao) :
        AsyncTask<Cart, Void, Void>() {

        override fun doInBackground(vararg params: Cart): Void? {
            mAsyncTaskDao.deleteItem(params[0])
            return null
        }
    }
}