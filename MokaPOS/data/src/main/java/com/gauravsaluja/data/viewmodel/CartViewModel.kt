package com.gauravsaluja.data.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.gauravsaluja.data.entity.Cart
import com.gauravsaluja.data.repository.CartDbRepository

/**
 * Created by Gaurav Saluja on 30-Nov-18.
 *
 * View model for cart items
 */

class CartViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository: CartDbRepository = CartDbRepository(application)
    private val allCartItems: LiveData<List<Cart>>?

    init {
        allCartItems = mRepository.allCartItemsDb
    }

    fun getAllCartItems() : LiveData<List<Cart>> {
        return allCartItems!!
    }
}